import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
public class p1 {

    public static void main(String[] args) {

        String csvFile = "/Users/Quree kumari/Desktop/mu/ilp-project/datasets_323_7768_matches.csv";
        String line = "";
        String cvsSplitBy = ",";        
        Map<String,Integer> m=new HashMap<String,Integer>();

        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {

            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] year = line.split(cvsSplitBy);
                if(m.containsKey(year[1]))
                  m.put(year[1],m.get(year[1])+1);                
                else
                  m.put(year[1],1);                                              
            }
            System.out.println("Year"+"        "+"total matches played");
            for(String s:m.keySet())            
            {
                if(s.equals("season"))
                    continue;
                System.out.println(s+"           "+m.get(s));
            }            
        }
         catch (IOException e) {
            e.printStackTrace();
        }

    }

}